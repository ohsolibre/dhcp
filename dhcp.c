#include <err.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <net/ethernet.h>

#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/if.h>
#include <linux/if_packet.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>

/* https://tools.ietf.org/html/rfc7844
   http://www.tcpipguide.com/free/t_DHCPMessageFormat.htm
   http://www.networksorcery.com/enp/protocol/dhcp.htm */
struct dhcp {
	uint8_t op;
	uint8_t htype;
	uint8_t hlen;
	uint8_t hops;
	uint32_t xid;
	uint16_t secs;
	uint16_t flags;
	uint32_t ciaddr;
	uint32_t yiaddr;
	uint32_t siaddr;
	uint32_t giaddr;
	uint8_t chaddr[16];
	uint8_t sname[64];
	uint8_t file[128];
	uint32_t cookie;
	uint8_t options[308];
};

struct packet {
	struct iphdr ip;
	struct udphdr udp;
	struct dhcp data;
};

uint8_t if_hwaddr[6];

static uint16_t checksum(void *p, int len)
{
	uint32_t sum = 0;

	for (; len > 0; len -= 2, p += 2)
		sum += *(uint16_t*)p;
	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	return (uint16_t)~sum;
}

static void get_interface(int sockfd, const char *name, int *index, uint8_t *hwaddr)
{
	struct ifreq ifr = {0};
	strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name));

	if (ioctl(sockfd, SIOCGIFINDEX, &ifr))
		err(1, "ioctl(SIOCGIFINDEX)");
	*index = ifr.ifr_ifindex;

	if (ioctl(sockfd, SIOCGIFHWADDR, &ifr))
		err(1, "ioctl(SIOCGIFHWADDR)");
	memcpy(hwaddr, ifr.ifr_hwaddr.sa_data, 6);
}

static void packet_init(struct packet *p)
{
	memset(&p->ip, 0, sizeof(p->ip));
	p->ip.version = IPVERSION;
	p->ip.ihl = 5;
	p->ip.ttl = IPDEFTTL;
	p->ip.protocol = IPPROTO_UDP;
	p->ip.saddr = INADDR_ANY;
	p->ip.daddr = INADDR_BROADCAST;
	p->ip.tot_len = htons(sizeof(*p));
	p->ip.check = checksum(&p->ip, sizeof(p->ip));

	memset(&p->udp, 0, sizeof(p->udp));
	p->udp.source = htons(68);
	p->udp.dest = htons(67);
	p->udp.len = htons(sizeof(p->udp) + sizeof(p->data));
}

static void dhcp_add_option(struct dhcp *d, uint opt, uint len, void *data)
{
	uint i;
	for (i = 0; i < sizeof(d->options) && d->options[i] != 0xff; i++)
		if (d->options[i])
			i += d->options[i+1] + 1;

	if (i + 2 + len < sizeof(d->options)) {
		d->options[i++] = opt;
		d->options[i++] = len;
		d->options[i+len] = 0xff;
		memcpy(d->options + i, data, len);
	}
}

static int dhcp_get_option(struct dhcp *d, uint opt, uint len, void *data)
{
	uint i;
	for (i = 0; i < sizeof(d->options) && d->options[i] != 0xff; i++) {
		if (d->options[i] == opt && i + 1 + len < sizeof(d->options)) {
			memcpy(data, &d->options[i+2], len);
			return 0;
		}
		if (d->options[i])
			i += d->options[i+1] + 1;
	}
	return -1;
}

static void dhcp_init(struct dhcp *d, uint8_t type)
{
	memset(d, 0, sizeof(*d));
	d->op = 1;    /* client to server request */
	d->htype = 1; /* ethernet */
	d->hlen = 6;  /* MAC len */
	d->cookie = htonl(0x63825363); /* magic cookie */
	memcpy(d->chaddr, if_hwaddr, 6);

	d->options[0] = 0xff;
	dhcp_add_option(d, 53, 1, &type); /* Message type */
}

static int dhcp_send_recv(int fd, struct sockaddr_ll *a, struct packet *out, struct packet *in)
{
	struct pollfd fds = {.fd = fd, .events = POLLIN};

	sendto(fd, out, sizeof(*out), 0, (struct sockaddr*)a, sizeof(*a));
	for (;;) {
		if (poll(&fds, 1, 1000) <= 0)
			return -1;
		if (read(fd, in, sizeof(*in)) <= 0)
			err(1, "read");

		if (in->ip.version == IPVERSION && in->ip.protocol == IPPROTO_UDP &&
		    in->udp.source == htons(67) && in->udp.dest == htons(68) &&
		    in->data.cookie == htonl(0x63825363) &&
		    checksum(&in->ip, sizeof(in->ip)) == 0)
			return 0;
	}
}

static void printip(char *prefix, uint32_t ip)
{
	uint8_t *p = (uint8_t*)&ip;
	printf("%s%u.%u.%u.%u\n", prefix, p[0], p[1], p[2], p[3]);
}

int main(int argc, char **argv)
{
	int fd;
	uint32_t ip;
	struct packet p, p1;
	struct sockaddr_ll addr = {
		.sll_family = PF_PACKET,
		.sll_protocol = htons(ETH_P_IP),
		.sll_halen = 6,
		.sll_addr = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	};

	if (argc < 2)
		errx(1, "usage: %s IFNAME", argv[0]);

	if ((fd = socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_IP))) < 0)
		err(1, "socket");

	get_interface(fd, argv[1], &addr.sll_ifindex, if_hwaddr);
	if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) < 0)
		err(1, "bind");

	packet_init(&p);
	for (;;) {
		dhcp_init(&p.data, 1); /* DHCPDISCOVER */
		if (dhcp_send_recv(fd, &addr, &p, &p1) < 0)
			continue;

		dhcp_init(&p.data, 3); /* DHCPREQUEST */
		memcpy(&p.data.siaddr, &p1.data.siaddr, sizeof(p.data.siaddr));
		dhcp_add_option(&p.data, 50, 4, &p1.data.yiaddr);
		if (dhcp_send_recv(fd, &addr, &p, &p1) == 0)
			break;
	}

	printip("IP=", p1.data.yiaddr);
	printip("SERVER=", p1.data.siaddr);
	if (!dhcp_get_option(&p1.data, 1, 4, &ip))
		printip("SUBNET=", ip);
	if (!dhcp_get_option(&p1.data, 28, 4, &ip))
		printip("BROADCAST=", ip);
}
