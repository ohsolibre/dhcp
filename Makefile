CC = cc
CFLAGS = -Wall -Wextra -Os
LDFLAGS = -s -static

dhcp: dhcp.c
	$(CC) -o $@ $< $(CFLAGS) $(LDFLAGS)
clean:
	rm -f dhcp
